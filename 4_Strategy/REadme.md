## Strategy pattern ## 

1. This code sample creates a Config class that expects formatStrategy input (see `config.js`)
2. Examine the code in the file
3. Create another strategy to load json configuration file from `configs/credentials.json`