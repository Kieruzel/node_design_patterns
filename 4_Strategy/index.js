import { Config } from "./config.js";
import { iniStrategy } from "./iniStrategy.js";


async function main() {
  const iniConfig = new Config(iniStrategy);
  await iniConfig.load("configs/credentials.ini");
  console.log(iniConfig.get('db.user'))
}
main();
