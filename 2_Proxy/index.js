import inquirer from 'inquirer';



class OneTimePassword {
  constructor() {
    this.password = Math.random();
    this.username = (Math.random() + 1).toString(36).substring(7);
  }
}


const credentials = new OneTimePassword()


async function retrieveCredentials() {
  try {
    const { choice } = await inquirer.prompt([
      {
        type: 'list',
        name: 'choice',
        message: 'What information would you like to retrieve?',
        choices: ['Username', 'Password'],
      }
    ]);

    if (choice === 'Username') {
      console.log(`Username: ${credentials.username}`);
    } else if (choice === 'Password') {
      const { confirm } = await inquirer.prompt([
        {
          type: 'confirm',
          name: 'confirm',
          message: 'Are you sure you want to reveal the password?',
        }
      ]);

      if (confirm) {
        console.log(`Password: ${credentials.password}`);
      } else {
        console.log('Operation cancelled.');
      }
    }
  } catch (error) {
    console.error('An error occurred:', error);
  }

  retrieveCredentials();
}

retrieveCredentials();


