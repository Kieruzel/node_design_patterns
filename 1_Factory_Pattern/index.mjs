const classList = {
  "positions": {
    "Goalkeeper": "Goalkeeper",
    "CentreBack": "CentreBack",
    "RightBack": "RightBack",
    "LeftBack": "LeftBack",
    "AttackingMidfielder": "AttackingMidfielder",
    "CentreMidfielder": "CentreMidfielder",
    "RightMidfielder": "RightMidfielder",
    "LeftMidfielder": "LeftMidfielder",
    "CentreForward": "CentreForward",
    "SecondaryStriker": "SecondaryStriker",
    "Striker": "Striker"
  }
}