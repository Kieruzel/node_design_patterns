## Factory Pattern ## 

1. Create a factory class to instantiate all the players from legendary Romainia team (1994)
2. Each separet classes for each position: Goalkeeper, CenterBack and so on

Hint, defining js classes dynamicly
``` javascript
const classes = {};

const classDefinitions = [
  { name: 'Class1', method: function() { console.log('Hello from Class1'); } },
  { name: 'Class2', method: function() { console.log('Hello from Class2'); } },
  { name: 'Class3', method: function() { console.log('Hello from Class3'); } }
];

classDefinitions.forEach(definition => {
  classes[definition.name] = class {
    constructor() {
      this.name = definition.name;
    }
    method() {
      definition.method();
    }
  }
});

// Usage
const instance1 = new classes.Class1();
instance1.method();  // Output: Hello from Class1

const instance2 = new classes.Class2();
instance2.method();  // Output: Hello from Class2

const instance3 = new classes.Class3();
instance3.method();  // Output: Hello from Class3
```

